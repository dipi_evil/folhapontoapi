﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;

namespace FolhaPontoService
{
    public class FolhaPontoService
    {
        private _Workbook pastaDeTrabalho =null;
        private _Worksheet planilha = null;
        private const string _valorParaMarcar = "X";
        private const string _naoFoiBatido = "NÃO FOI BATIDO";

        public void FolhaPontoService(string arquivoFolhaPonto = "folhaponto.xlsx")
        {
            Application ExcelApp = new Application();
            ExcelApp.Visible = true;

            pastaDeTrabalho = ExcelApp.Workbooks.Open("App_Data"+arquivoFolhaPonto);
            planilha = (_Worksheet)pastaDeTrabalho.ActiveSheet;
        }

        public Tuple<string, string, string, string> BuscarValoresDia(string dia)
        {
            string E1; string S1; string E2; string S2;

            E1 = S1 = E2 = S2 = _naoFoiBatido;

            int nLinha = BuscarLinhaDoDia(dia);

            E1 = (planilha.Cells[nLinha, 1] != string.Empty) ? planilha.Cells[nLinha, 2] : _naoFoiBatido;
            S1 = (planilha.Cells[nLinha, 1] != string.Empty) ? planilha.Cells[nLinha, 3] : _naoFoiBatido;
            E2 = (planilha.Cells[nLinha, 1] != string.Empty) ? planilha.Cells[nLinha, 4] : _naoFoiBatido;
            S2 = (planilha.Cells[nLinha, 1] != string.Empty) ? planilha.Cells[nLinha, 5] : _naoFoiBatido;

            return new Tuple<string, string, string, string>(E1,S1,E2,S2);
        }

        public void SalvarHorario()
        {
            DateTime dtAgora = DateTime.Now;

            string strData = dtAgora.ToString("dd\\MM\\yyyy");
            string strHora = dtAgora.ToString("hh:mm:ss");

            int nLinha = BuscarLinhaDoDia(strData);
            int nCol = 0;

            if(dtAgora.Hour < 10)
                nCol = 2;
            else if(dtAgora.Hour >= 10 || (dtAgora.Hour < 13 || dtAgora.Minute < 30))                    
                nCol = 3;
            else if(dtAgora.Hour == 13 && dtAgora.Minute <= 30 || dtAgora.Hour > 14 && dtAgora.Hour < 17)                    
                nCol = 4;
            else if(dtAgora.Hour > 17)
                nCol = 5;                    

            planilha.Cells[nLinha, nCol] = _valorParaMarcar;
            
            SalvarPlanilha();
        }

        #region Métodos privados

        private void SalvarPlanilha()
        {
            pastaDeTrabalho.Save();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strData"></param>
        /// 
        /// <returns></returns>
        private int BuscarLinhaDoDia(string strData)
        {
            int nLinha = 0;
            while (nLinha < 32 )
            {
                if (planilha.Cells[nLinha, 1] == strData)                
                    break;                
                nLinha++;
                if (nLinha == 31)
                    throw new Exception("Não existe a data correspondente");
            }
            return nLinha;
        }
        #endregion

        public void ExportarPlanilha()
        {

        }

    }
}
