﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class AcoesController : ApiController
    {
        // GET api/acoes
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/acoes/5
        public string Get(string dia)
        {

            return "value";
        }

        // POST api/acoes
        public void Post([FromBody]string value)
        {
            
        }

        // PUT api/acoes/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/acoes/5
        public void Delete(int id)
        {
        }
    }
}
